import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = ['https://www.googleapis.com/auth/calendar.events']


def main():
    creds = None
    content = {
        "summary": "Python API",
        "description": "Developers, Developers!",
        "location": "Lago Valencia 75, Argentina Antigua, 11270 Ciudad de México",
        "start": {
            "dateTime": "2019-09-25T20:00:00-05:00",
            "timeZone": "America/Mexico_City",
        },
        "end": {
            "dateTime": "2019-09-25T21:00:00-05:00",
            "timeZone": "America/Mexico_City",
        },
        'reminders': {
            'useDefault': True,
        },
    }
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'client_id.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('calendar', 'v3', credentials=creds)
    try:
        events_result = service.events().insert(
            calendarId="primary", body=content, sendUpdates="all").execute()
        event_name = events_result.get('summary', [])
        print(f"Evento {event_name} Creado")
    except Exception:
        pass


if __name__ == '__main__':
    main()
