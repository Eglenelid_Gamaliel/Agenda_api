# Medisel Google Calendar API

## Introduccion

La función de este proyecto es facilitar la sincronización de la Agenda Médica Medisel con el servicio de Calendario de Google utilizando su implementación para Python 3 y Django Rest Framework.

La aplicación funciona detrás del servidor WSGI CherryPy, un servidor HTTP 1.1 de alta velocidad, lo que garantiza el correcto rendimiento para el despliegue en producción.

Compatibilidad:

- Windows Server 2008 R2 o superior.

- Linux.
  - Arch Linux.
  - Debían.

## Requisitos

- Python 3.7+ x64
  - Windows

    [<https://www.python.org/ftp/python/3.8.1/python-3.8.1-amd64.exe>](#)

    Es necesario ejecutar el instalador como administrador y seleccionar la opción de "Add Python to PATH".

- Google Cloud Platform
  
  - ID de clientes OAuth 2.0
  
    En el menu "API y servicios" debera crear nuevas credenciales de tipo "ID de cliente de OAuth" donde el tipo de aplicacion será "aplicacion web". Los "campos Orígenes autorizados de JavaScript" y "URI de redireccionamiento autorizados" deberan contener la direccion donde será desplegada la aplicacion.

    `http://dominio:8010`

## Instalación

1. Clonar el repositorio al almacenamiento local.

    Windows.

    Abra una ventana de Powershell como administrador e introduzca los siguientes comandos:

   ​ `cd C:\inetpub\wwwroot`

    `git clone https://gitlab.com/TheBlueAndroid/Agenda_api`

    `cd Agenda_api`

    En caso de no contar con las herramientas de git, debera descargar y descomprimir el repositorio manualmente en la carpeta "C:\inetpub\wwwroot", luego debera abrir una ventana de Powershell dentro de la carpeta del proyecto.

2. Crear un entorno virtual de Python para contener el proyecto.

    `python -m venv venv`
    
2.1 Activarlo tecleando:

    `.\venv\scripts\activate`

3. Instalar las dependencias.

    `pip install -r requirements.txt`

4. Iniciar el servidor de desarrollo para verificar la instalación.

    `python manage.py runserver 127.0.0.1:80`

    En este caso el parámetro "127.0.0.1:80" iniciará el servidor de desarrollo de manera local en el equipo. Deberá ser modificado para funcionar en la dirección IP y puertos deseados para la prueba de instalación.

## Despliegue

1. En la carpeta del proyecto deberá editarse el archivo "Agenda_api/settings.py"  

    `ALLOWED_HOSTS = [""]`

    Esta línea deberá reflejar las direcciones desde donde la aplicación podrá ser llamada.

    Por ejemplo:

    `ALLOWED_HOSTS = ["localhost", "130.130.130.80", "dominio.com"]`

    Debera hacer lo mismo con el archivo "server_service.bat" en la linea:

    `call daphne -b 130.130.130.80 -p 8080 Agenda_api.asgi:application`

    Siendo "-b" la direccion IP y "-p" el puerto deseados.

    Para la conexion con los servicios de Google deberá configurar las credenciales de Google OAuth, desde el menu de su proyecto de Google descargue sus credenciales en formato JSON y coloque dicho archivo en la carpeta "C:\inetpub\wwwroot\Agenda_api".

    Luego modifique la siguiente linea de codigo.

    `GOOGLE_CLIENT_SECRET_FILE = os.path.join(BASE_DIR, 'credentials.json')`

    Modifique la cadena 'credentials.json' deacuerdo al nombre de su archivo de credenciales.

    Por ejemplo:

    `GOOGLE_CLIENT_SECRET_FILE = os.path.join(BASE_DIR, 'client_secret.json')`

    Finalmente edite la siguiente linea de codigo con su ID de cliente (Deberia estar disponible en la misma pagina donde descargo anteriormente su archivo JSON)

    `GOOGLE_CLIENT_ID = "xxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com"`

    Por ejemplo:

    `GOOGLE_CLIENT_ID = "4eef4258dba-fjgur488s125f8c94ms.apps.googleusercontent.com"`

2. Se instalará una instancia del proyecto como servicio en el panel de servicios de Windows:

    `nssm install "Medisel Google Calendar API"`

    `nssm set "Medisel Google Calendar API" Application C:\inetpub\wwwroot\Agenda_api\server_service.bat`

    `nssm set "Medisel Google Calendar API" AppDirectory C:\inetpub\wwwroot\Agenda_api`

    Este servicio dependerá de los archvos de este repositorio y se iniciará automáticamente durante el arranque del sistema, puede ser encontrado en los servicios de Windows como "Medisel Google Calendar API"

## Uso

Para iniciar sesion en Google y obtener acceso a los calendarios del usuario bastará con dirigirlo a la siguiente URL.

- /login/<user_id>

Donde <user_id> es una cadena que identifica al usuario, por ejemplo: "/login/87958379".

Las peticiones son dee tipo POST, deben ser envidas en formato JSON y se distribuyen en los siguientes endpoints:

- /insert
  
  Añade un evento al calendario, recibe peticiones con el siguiente formato.

  `{
    "Internal_User_Id": "identificador unico del usuario",
    "Internal_Event_Id": "identificador unico del evento",
    "body": {
      "summary": "Titulo de evento",
      "description": "Descripcion del evento",
      "start": {
        "dateTime": "YYYY-MM-DDTHH:MM:SS-05:00",
        "timeZone": "America/Mexico_City"
      },
      "end": {
        "dateTime": "YYYY-MM-DDTHH:MM:SS-05:00",
        "timeZone": "America/Mexico_City"
      }
    }
  }`

  Por ejemplo:

  `{
    "Internal_User_Id": "1",
    "Internal_Event_Id": "1",
    "body": {
      "summary": "Evento Prueba",
      "description": "Descripcion del evento",
      "start": {
        "dateTime": "2019-10-25T18:30:00-05:00",
        "timeZone": "America/Mexico_City"
      },
      "end": {
        "dateTime": "2019-10-25T18:30:00-05:00",
        "timeZone": "America/Mexico_City"
      }
    }
  }`

- /edit
  
  Modifica un evento.

  Este endpoint comparte su formato con /insert

- /delete
  
  Remueve un evento del calendario, recibe peticiones con el siguiente formato.

  `{
    "Internal_User_Id": "identificador unico del usuario",
    "Internal_Event_Id": "identificador unico del evento",
  }`

  `{
    "Internal_User_Id": "1",
    "Internal_Event_Id": "1",
  }`
