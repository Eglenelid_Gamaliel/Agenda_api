from django.contrib import admin
from .models import Users, Events


@admin.register(Users)
class UsersAdmin(admin.ModelAdmin):
    pass


@admin.register(Events)
class EventsAdmin(admin.ModelAdmin):
    pass
