from django.db import IntegrityError
from django.views.decorators.csrf import ensure_csrf_cookie
from django.conf import settings
from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django_google.flow import DjangoFlow, CLIENT_SECRET_FILE, SCOPES
from googleapiclient.discovery import build
from .models import Users, Events


@ensure_csrf_cookie
def oAuthJavascriptView(request, user_id):

    calendar_insert_body = {
        "summary": "Medisel",
        "description": "En este calendario encontrará los eventos sincronizados desde la plataforma Medisel",
    }

    user_is_logged = False
    if request.is_ajax():
        if request.method == "POST":
            code = request.POST.get("code")
            flow = DjangoFlow.from_client_secrets_file(
                client_secrets_file=CLIENT_SECRET_FILE, scopes=SCOPES
            )
            creds = flow.get_credentials_from_code(
                code=code, javascript_callback_url="postmessage"
            )
            print(creds)
            try:
                user = Users.objects.get(user_id=user_id)
                user.user_credentials = creds
                user.save()
                service = build("calendar", "v3", credentials=creds)
                calendar_insert = (
                    service.calendarList()
                    .insert(body={"id": f"{user.calendar_id}"})
                    .execute()
                )

                return JsonResponse({}, status=200)
            except Users.DoesNotExist:
                service = build("calendar", "v3", credentials=creds)
                calendar_insert = (
                    service.calendars().insert(body=calendar_insert_body).execute()
                )
                Users.objects.create(
                    user_id=user_id,
                    user_credentials=creds,
                    calendar_id=calendar_insert["id"],
                )
                # return JSON Response with Status Code of 200 for success and 400 for errors
                return JsonResponse({}, status=200)
        if request.method == "DELETE":
            user = Users.objects.get(user_id=user_id)
            service = build("calendar", "v3", credentials=user.user_credentials)
            calendar_delete = (
                service.calendarList().delete(calendarId=user.calendar_id).execute()
            )
            user.user_credentials = ""
            user.save()
            return JsonResponse({}, status=200)
    else:
        try:
            user = Users.objects.get(user_id=user_id)
            if user.user_credentials:
                user_is_logged = True
            else:
                user_is_logged = False
        except Users.DoesNotExist:
            user_is_logged = False

        context = {
            "client_id": getattr(settings, "GOOGLE_CLIENT_ID", None),
            "scopes": " ".join(SCOPES),
            "user_is_logged": user_is_logged,
        }
        # Render HTML page that havs Google Authentication Page with Javasccript
        return render(request, "login.html", context)


@api_view(["GET", "POST"])
def create_event(request):
    """
    Create the desired event
    """
    if request.method == "GET":
        return Response(status.HTTP_200_OK)

    elif request.method == "POST":
        json_string = request.data
        try:
            InternalUserId = json_string["Internal_User_Id"]
            InternalEventId = json_string["Internal_Event_Id"]
            GoogleCredentials = Users.objects.get(user_id=InternalUserId)
            try:
                service = build(
                    "calendar", "v3", credentials=GoogleCredentials.user_credentials
                )
                Events.objects.create(
                    author=GoogleCredentials, internalId=InternalEventId
                )
                event_insert = (
                    service.events()
                    .insert(
                        calendarId=GoogleCredentials.calendar_id,
                        body=json_string["body"],
                        sendUpdates="all",
                    )
                    .execute()
                )
                Events.objects.filter(
                    author=GoogleCredentials, internalId=InternalEventId
                ).update(eventId=event_insert.get("id"))
                event_name = event_insert.get("summary")
                print(f"Evento '{event_name}' Creado")
                return Response(status=status.HTTP_200_OK)
            except IntegrityError:
                return Response(status=status.HTTP_202_ACCEPTED)
        except Users.DoesNotExist as error:
            print(error)
            return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET", "POST"])
def edit_event(request):
    """
    Modify the desired event
    """
    if request.method == "GET":
        return Response(status.HTTP_200_OK)

    elif request.method == "POST":
        json_string = request.data
        try:
            InternalUserId = json_string["Internal_User_Id"]
            InternalEventId = json_string["Internal_Event_Id"]
            GoogleCredentials = Users.objects.get(user_id=InternalUserId)
            try:
                eventId = Events.objects.get(internalId=InternalEventId).eventId
                service = build(
                    "calendar", "v3", credentials=GoogleCredentials.user_credentials
                )
                update_event = (
                    service.events()
                    .update(
                        calendarId=GoogleCredentials.calendar_id,
                        eventId=eventId,
                        body=json_string["body"],
                        sendUpdates="all",
                    )
                    .execute()
                )
                event_name = update_event.get("summary")
                print(f"Evento {event_name} Actualizado")
                return Response(status=status.HTTP_200_OK)
            except Events.DoesNotExist:
                return Response(status=status.HTTP_200_OK)
        except Users.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET", "POST"])
def delete_event(request):
    """
    Delete the desired event
    """
    if request.method == "GET":
        return Response(status.HTTP_200_OK)

    elif request.method == "POST":
        json_string = request.data
        try:
            InternalUserId = json_string["Internal_User_Id"]
            InternalEventId = json_string["Internal_Event_Id"]
            GoogleCredentials = Users.objects.get(user_id=InternalUserId)
            try:
                eventId = Events.objects.get(internalId=InternalEventId).eventId
                service = build(
                    "calendar", "v3", credentials=GoogleCredentials.user_credentials
                )
                service.events().delete(
                    calendarId=GoogleCredentials.calendar_id,
                    eventId=eventId,
                    sendUpdates="all",
                ).execute()
                Events.objects.get(eventId=eventId).delete()
                print(f"Evento Eliminado")
                return Response(status=status.HTTP_200_OK)
            except Events.DoesNotExist:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        except Users.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)
