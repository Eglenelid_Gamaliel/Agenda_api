from django.urls import path
from .views import oAuthJavascriptView, create_event, edit_event, delete_event

urlpatterns = [
    path('login/<str:user_id>', oAuthJavascriptView, name="login"),
    path('insert', create_event, name="insert_event"),
    path('edit', edit_event, name="edit_event"),
    path('delete', delete_event, name="delete_event"),
]
