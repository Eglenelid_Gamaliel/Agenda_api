"""
Define la base de datos auxiliar para almacenar las credenciales de acceso a la api
"""
import pickle
from django.db import models
from google.auth.transport.requests import Request


class Users(models.Model):
    user_id = models.CharField(max_length=30, unique=True)
    _user_credentials = models.BinaryField()
    calendar_id = models.CharField(max_length=40, null=True)

    def set_data(self, data):
        self._user_credentials = pickle.dumps(data)

    def get_data(self):
        credentials = pickle.loads(self._user_credentials)
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
            self._user_credentials = pickle.dumps(credentials)
            self.save()
        return pickle.loads(self._user_credentials)

    user_credentials = property(get_data, set_data)

    def __str__(self):
        return self.user_id

    class Meta:
        verbose_name = "Google Authentication"
        verbose_name_plural = "Google Authentications"


class Events(models.Model):
    internalId = models.CharField(max_length=100, primary_key=True)
    eventId = models.CharField(max_length=30)
    author = models.ForeignKey(Users, on_delete=models.CASCADE, to_field='user_id')
    

    def __str__(self):
        return self.author.user_id

    class Meta:
        verbose_name = "Google Event"
        verbose_name_plural = "Google Events"
